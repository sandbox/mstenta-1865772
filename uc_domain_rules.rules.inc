<?php

/**
 * @file
 * Ubercart Domain Rules
 */

/**
 * Implementation of hook_rules_condition_info().
 * @ingroup rules
 */
function uc_domain_rules_rules_condition_info() {
  return array(
    'uc_domain_rules_condition_order_domain' => array(
      'label' => t('Order belongs to a domain'),
      'help' => t('TRUE is returned, if an order belongs to a specified domain.'),
      'arguments' => array(
        'order_id' => array('label' => t('Order ID'), 'type' => 'number'),
        'domains' => array('label' => t('Domains'), 'type' => 'value'),
      ),
      'module' => 'Ubercart Domain ',
    ),
  );
}

/**
 * Settings form for the "Order belongs to a domain" condition.
 */
function uc_domain_rules_condition_order_domain_form($settings, &$form) {

  // Add a textfield for the order id.
  $settings += array('order_id' => '');
  $form['settings']['order_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Order ID'),
    '#default_value' => $settings['order_id'],
    '#required' => TRUE,
  );

  // Build a list of domain options (code courtesy of Domain Context module).
  $values = array();
  $format = domain_select_format();
  foreach (domain_domains() as $data) {
    ($data['domain_id'] == 0) ? $key = -1 : $key = $data['domain_id'];
    if ($data['valid'] || user_access('access inactive domains')) {
      $values[$key] = empty($format) ? check_plain($data['sitename']) : $data['sitename'];
    }
  }

  // Add a domain checkbox selection to the form.
  $settings += array('domains' => array());
  $form['settings']['domains'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Domains'),
    '#options' => $values,
    '#default_value' => $settings['domains'],
    '#required' => TRUE,
  );
}

/**
 * Callback function for the "Order belongs to a domain" condition.
 */
function uc_domain_rules_condition_order_domain($order_id, $domains, $settings) {

  // Load the order.
  $order = uc_order_load($order_id);

  // Bail if the order wasn't found.
  if (!$order) {
    return FALSE;
  }

  // Get the order's domain. If it's 0, translate it to -1 (both mean the default domain).
  $domain_id = $order->domain_id ? $order->domain_id : -1;

  // If the order is in one of the selected domains, return TRUE.
  if (isset($domains[$domain_id]) && $domains[$domain_id] == $domain_id) {
    return TRUE;
  }

  // Otherwise, return FALSE.
  else {
    return FALSE;
  }
}
